import type { ComponentOptionsMixin, DefineComponent, ExtractPropTypes, PublicProps } from 'vue'
import { TypeItemInventory } from '@/@types/Invenory'

/**
 * Interface representing an inventory menu item.
 * @interface
 * @property {number} id - Unique identifier for the menu item.
 * @property {string} name - The name of the menu item.
 */
export interface InventoryMenuItem {
  id: number
  name: string
}

/**
 * Enum representing types of filters.
 * @enum {string}
 */
export enum FiltersType {
  ALL = 'all',
  MISC = TypeItemInventory['MISC'],
  WEAPON = TypeItemInventory['WEAPON'],
  ARMOR = TypeItemInventory['ARMOR']
}

/**
 * Interface representing a sidebar menu item.
 * @interface
 * @property {DefineComponent} component - The Vue component to be rendered.
 * @property {FiltersType} type - The type of filter the menu item represents.
 * @property {string} title - The title of the menu item.
 */
export interface SidebarMenuItem {
  component: DefineComponent<
    {},
    {},
    {},
    {},
    {},
    ComponentOptionsMixin,
    ComponentOptionsMixin,
    {},
    string,
    PublicProps,
    Readonly<ExtractPropTypes<{}>>,
    {},
    {}
  >
  type: FiltersType
  title: string
}
