/**
 * Enum for identifying the type of inventory items.
 */
export enum TypeItemInventory {
  MISC = 'misc',
  WEAPON = 'weapon',
  ARMOR = 'armor',
  EMPTY = 'empty'
}

/**
 * Base interface for all types of inventory items.
 * @interface
 * @property {string} id - Unique identifier for the inventory item.
 * @property {TypeItemInventory} type - Type of the inventory item, as defined by the TypeItemInventory enum.
 * @property {string} name - The name of the inventory item.
 * @property {string} imageUrl - URL to an image of the inventory item.
 */
export interface InventoryItemBase {
  id: string
  type: TypeItemInventory
  name: string
  imageUrl: string
}

/**
 * Interface for miscellaneous items in the inventory.
 * Extends InventoryItemBase with additional properties specific to miscellaneous items.
 * @interface
 * @property {number} count - The quantity of the item available.
 * @property {number} [charges] - Current number of uses or charges left (optional).
 * @property {number} [maxCharges] - Maximum number of charges the item can hold (optional).
 * @property {number} [cooldown] - Cooldown in seconds before the item can be used again (optional).
 */
interface InventoryItemMisc extends InventoryItemBase {
  count: number
  charges?: number
  maxCharges?: number
  cooldown?: number
}
interface InventoryItemEmpty extends Pick<InventoryItemBase, 'type' | 'id'> {}

/**
 * Interface for weapons in the inventory.
 * Extends InventoryItemBase, may include methods or properties specific to weapons in the future.
 * @interface
 */
interface InventoryItemWeapon extends InventoryItemBase {
  // Potential future properties or methods for weapon-specific functionality
}

/**
 * Interface for armor items in the inventory.
 * Extends InventoryItemBase, may include methods or properties specific to armor in the future.
 * @interface
 */
interface InventoryItemArmor extends InventoryItemBase {
  // Potential future properties or methods for armor-specific functionality
}

/**
 * Maps the type of item from the TypeItemInventory enum to the corresponding interface.
 */
type InventoryTypeMap = {
  [TypeItemInventory.MISC]: InventoryItemMisc
  [TypeItemInventory.WEAPON]: InventoryItemWeapon
  [TypeItemInventory.ARMOR]: InventoryItemArmor
  [TypeItemInventory.EMPTY]: InventoryItemEmpty
}

/**
 * Generic type that provides the specific interface corresponding to the TypeItemInventory enum value.
 * Used to ensure type safety across functions and components that handle inventory items.
 * @template T - Enum type from TypeItemInventory specifying the type of the item.
 */
export type InventoryItemByType<T extends TypeItemInventory> = InventoryTypeMap[T]

/**
 * Represents any possible inventory item from the inventory type map, allowing for generic handling
 * of inventory items. This type is suitable for functions or components that operate on inventory
 * items but do not need to distinguish between the specific types of items.
 */
export type AnyInventoryItem =
  | InventoryItemMisc
  | InventoryItemWeapon
  | InventoryItemArmor
  | InventoryItemEmpty
