import axios, { AxiosError, type AxiosResponse } from 'axios'
import type { AnyInventoryItem } from '@/@types/Invenory'

const instance = axios.create({
  baseURL: import.meta.env.VITE_API_URL
})

instance.interceptors.response.use(
  (response) => response,
  (error: AxiosError) => {
    // TODO можно периписать на нормальный обработчик
    alert(`При попытке запроса :\n ${error.request.responseURL} \n произошла ошибка!`)
    return error
  }
)

export default {
  async getItems(id: number = 1): Promise<AnyInventoryItem[]> {
    const data: AxiosResponse<{ inventory: AnyInventoryItem[] }> = await instance.get(
      'vueDevTestTask-getInventoryState',
      {
        params: {
          case: id
        }
      }
    )
    return data.data?.inventory || []
  }
}
