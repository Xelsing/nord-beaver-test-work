import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/views/index/IndexPage.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
      props: (route) => ({ stateInventory: route.query.case || '1' })
    }
  ]
})

export default router
