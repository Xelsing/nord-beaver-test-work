/** @type {import('tailwindcss').Config} */
export default {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  content: [
    './src/**/*.{vue}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
  prefix: 'tw-'
}
